import React from 'react';
import MainRoute from './routes';
import './App.scss';

const App = () => {
  return <MainRoute />;
};

export default App;
